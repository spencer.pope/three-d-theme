import React, { useMemo, useContext, useEffect } from 'react';
import { useThree } from 'react-three-fiber';
import { ThemeContext } from 'styled-components';

function Text({ children, position, opacity = 1, color = 'white', fontSize = 40 }) {
  const theme = useContext(ThemeContext);
  const {
    size: { width, height },
    viewport: { width: viewportWidth, height: viewportHeight },
    gl,
  } = useThree();

  useEffect(() => {
    gl.setClearColor(theme.background, 1);
  }, [gl, theme]);

  const scale = viewportWidth > viewportHeight ? viewportWidth : viewportHeight;
  const canvas = useMemo(() => {
    const canvas = document.createElement('canvas');
    canvas.width = canvas.height = 2048;
    const context = canvas.getContext('2d');
    context.font = `bold ${fontSize}px -apple-system, helvetica, segoe ui, sans-serif`;
    context.textAlign = 'left';
    context.textBaseline = 'middle';
    context.fillStyle = theme.color;
    const maxWidth = canvas.width / 3;
    const words = children.split(' ');
    let line = '';
    const lineHeight = fontSize + 5;
    let y = lineHeight;

    words.map((word, i) => {
      var testLine = line + words[i] + ' ';
      var metrics = context.measureText(testLine);
      var testWidth = metrics.width;
      return testWidth > maxWidth
        ? (context.fillText(line, canvas.width / 2, canvas.width / 2 + y), (line = words[i] + ' '), (y += lineHeight))
        : (line = testLine);
    });
    context.fillText(line, canvas.width / 2, canvas.width / 2 + y);

    return canvas;
  }, [fontSize, theme.color, children]);
  return (
    <sprite scale={[scale, scale, 1]} position={[-viewportWidth / 2 + 0.2, viewportHeight / 2, 0]}>
      <spriteMaterial attach="material" transparent opacity={opacity}>
        <canvasTexture attach="map" image={canvas} premultiplyAlpha onUpdate={s => (s.needsUpdate = true)} />
      </spriteMaterial>
    </sprite>
  );
}

export default Text;
