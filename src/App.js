import React, { useState } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import Scene from './Blob/Scene';
import useMotion, { normalize } from './useMotion';

const Main = styled.main`
  height: calc(100vh - 20px);
  color: ${({ theme }) => theme.color};
  background: ${({ theme }) => theme.background};
`;

const themes = {
  light: {
    color: '#000000',
    background: '#ffffff',
  },
  dark: {
    color: '#ffffff',
    background: '#000000',
  },
};

function App() {
  const [theme, setTheme] = useState('dark');
  const { alpha, beta } = useMotion();
  return (
    <ThemeProvider theme={themes[theme]}>
      <Main>
        <button onClick={() => setTheme('dark')}>dark</button>
        <button onClick={() => setTheme('light')}>light</button>
        <h1>{normalize(alpha)}</h1>
        <h1>{normalize(beta, 0, 1)}</h1>
        <Scene />
      </Main>
    </ThemeProvider>
  );
}

export default App;
