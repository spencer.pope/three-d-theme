import React, { useState, useEffect } from 'react';

export const normalize = (val, min = 0, max = 4, inputMin = -180, inputMax = 180) =>
  ((val - inputMin) / (inputMax - inputMin)) * (max - min) + min;

export default function useMotion() {
  const [info, setInfo] = useState({ alpha: 0, beta: 0, gamma: 0 });
  useEffect(() => {
    const setValue = ({ alpha, beta, gamma }) => setInfo({ alpha, beta, gamma });
    window.addEventListener('deviceorientation', setValue);

    return () => window.removeEventListener('deviceorientation', setValue);
  }, []);
  return info;
}
